#!/bin/bash
name=gcc
version=`cat gcc/BASE-VER`
tarballs=$name-$version-`date +%Y%m%d`.tar.xz

git archive --format=tar.xz --prefix=$name-$version/ -o $tarballs la64/gcc-12/release

if mountpoint /archlinux/ >/dev/null 2>&1;then
    cp $tarballs /archlinux/sources
fi
